# Data Management Lab

## Synopsis
This repository holds a Docker Compose environment with applications, services and tools for the Data Management Lab of the University of Milano-Bicocca Master Degree in Data Science.

All the brief descriptions of the available services are extracted from their official websites. Apache Hadoop, Apache HBase and Apache Hive Docker images and configurations are extracted from the [Big Data Europe Project](https://github.com/big-data-europe).

## Applications, Services and Tools

### Apache Hadoop v2.7.4
The Apache Hadoop software library is a framework that allows for the distributed processing of large data sets across clusters of computers using simple programming models.

It is designed to scale up from single servers to thousands of machines, each offering local computation and storage.

* HDFS is the distributed file system that provides high-throughput access to application data
* YARN is a framework for job scheduling and cluster resource management
* MapReduce is a YARN-based system for parallel processing of large data sets

**Service name**: _hadoop_

**Service dependencies**: _none_

**Components**:

| Component              | Exposed Port  |
|------------------------|---------------|
| hadoop-namenode        | 50700         |
| hadoop-datanode        | 50075         |
| hadoop-resourcemanager | 8088          |
| hadoop-nodemanager     | 8042          |
| hadoop-historyserver   | 8188          |

### Apache HBase v1.2.6
Apache HBase is a distributed and scalable big data store. It was modeled after Google's Bigtable. Just as Bigtable leverages the distributed data storage provided by the Google File System, Apache HBase provides Bigtable-like capabilities on top of Hadoop and HDFS.

Use Apache HBase when you need random, realtime read/write access to your Big Data. This project's goal is the hosting of very large tables (billions of rows X millions of columns) atop clusters of commodity hardware.

**Service name**: _hbase_

**Service dependencies**: _zookeeper_, _hadoop_

**Components**:

| Component    | Exposed Port  |
|--------------|---------------|
| hbase-master | 16010         |
| hbase-region | 16030         |

**Useful resources**

* [HBase 1.2 Reference Book](https://hbase.apache.org/1.2/book.html)

### Apache Hive v2.3.2

The Apache Hive data warehouse software facilitates reading, writing, and managing large datasets residing in distributed storage and queried using SQL syntax.

Hive provides standard SQL functionality, including many of the later SQL:2003, SQL:2011, and SQL:2016 features for analytics.

Built on top of Apache Hadoop, Hive provides features like:

* Tools to enable easy access to data via SQL, thus enabling data warehousing tasks such as extract/transform/load (ETL), reporting, and data analysis
* A mechanism to impose structure on a variety of data formats
* Access to files stored either directly in Apache HDFS or in other data storage systems such as Apache HBase
* Many others...

**Service name**: _hive_

**Service dependencies**: _hadoop_

**Components**:

| Component                 | Exposed Port  |
|---------------------------|---------------|
| hive-server               | 10000         |
| hive-mestastore           | 9083          |
| hive-metastore-postgresql |               |

**Useful resources**

* [User Documentation](https://cwiki.apache.org/confluence/display/Hive/Home#Home-UserDocumentation)

### Apache ZooKeeper v3.4.10
Apache ZooKeeper is a high-performance coordination service for distributed applications. It exposes common services - such as naming, configuration management, synchronization, and group services - in a simple interface so you don't have to write them from scratch. You can use it off-the-shelf to implement consensus, group management, leader election, and presence protocols. And you can build on it for your own, specific needs.

**Service name**: _zookeeper_

**Service dependencies**: _none_

**Components**:

| Component | Exposed Port  |
|-----------|---------------|
| zookeeper | 2181          |

### Apache Kafka v2.3.0
Apache Kafka is a distributed streaming platform. It has three key capabilities:

* Publish and subscribe to streams of records, similar to a message queue or enterprise messaging system
* Store streams of records in a fault-tolerant durable way
* Process streams of records as they occur

Apache Kafka is generally used for two broad classes of applications:

* Building real-time streaming data pipelines that reliably get data between systems or applications
* Building real-time streaming applications that transform or react to the streams of data

**Service name**: _kafka_

**Service dependencies**: _zookeeper_

**Components**:

| Component | Exposed Port  |
|-----------|---------------|
| kafka     | 9092          |

**Useful resources**
* [Getting Started](https://kafka.apache.org/documentation/#gettingStarted)

### Apache Nifi v1.10
Apache NiFi is an easy to use, powerful, and reliable system to process and distribute data.
It was made for dataflow and supports highly configurable directed graphs of data routing, transformation, and system mediation logic.

**Service name**: _nifi_

**Service dependencies**: _none_

**Components**:

| Component | Exposed Port  |
|-----------|---------------|
| nifi      | 8080, 10001   |

**Useful resources**
* [User Guide](https://nifi.apache.org/docs/nifi-docs/html/user-guide.html)

* [Documentation](https://nifi.apache.org/docs.html)

### MongoDB v4.2
MongoDB is a free and open-source cross-platform document-oriented database program. Classified as a NoSQL database program, MongoDB uses JSON-like documents with schema.

Mongo Express is web-based MongoDB admin interface.

**Service name**: _mongo_

**Service dependencies**: _none_

**Components**:

| Component     | Exposed Port  | Default Credentials  |
|---------------|---------------|----------------------|
| mongo         | 27017         | admin / DataMan2019! |
| mongo-express | 8081          | admin / DataMan2019! |

**Useful resources**
* [Create-Read-Update-Delete (CRUD) Operations](https://docs.mongodb.com/manual/crud/)

### Neo4j v3.5

Neo4j is a highly scalable native graph database and processing engine.

Cypher is a vendor-neutral open graph query language used by Neo4j. Its ASCII-art style syntax provides a familiar, readable way to match patterns of nodes and relationships within graph datasets.

Like SQL, Cypher is a declarative query language that allows users to state what actions they want performed (such as match, insert, update or delete) upon their graph data without requiring them to describe (or program) exactly how to do it.

**Service name**: _neo4j_

**Service dependencies**: _none_

**Components**:

| Component | Exposed Port  | Default Credentials |
|-----------|---------------|---------------------|
| neo4j     | 7474, 7687    | neo4j / neo4j       |

**Useful resources**

* [Graph Database Concepts](https://neo4j.com/docs/getting-started/current/graphdb-concepts/)
* [Cypher Manual](https://neo4j.com/docs/cypher-manual/current/)

### ArangoDB v3.5
ArangoDB is a native multi-model, open-source database with flexible data models for documents, graphs, and key-values.

**Service name**: _arangodb_

**Service dependencies**: _none_

**Components**:

| Component | Exposed Port  | Default Credentials |
|-----------|---------------|---------------------|
| arangodb  | 8529          | root / DataMan2019! |

**Useful resources**

* [Documentation](https://www.arangodb.com/docs/stable/index.html)

### JupyterLab (Datascience-notebook release)
JupyterLab enables you to work with documents and activities such as Jupyter notebooks, text editors, terminals, and custom components in a flexible, integrated, and extensible manner. You can arrange multiple documents and activities side by side in the work area using tabs and splitters.

The Datascience-notebook release includes libraries for data analysis from the Julia, Python, and R communities. See [here](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-datascience-notebook) what it is already in!

**Service name**: _jupyter-lab_

**Service dependencies**: _none_

**Components**:

| Component   | Exposed Port  |
|-------------|---------------|
| jupyter-lab | 8888          |

## How to start services

The first time you start a service, it may need to download its Docker image. This could take up to few minutes. Next times, it will be faster.
```sh
./datalab-cli.sh -a start -s serviceName1 -s serviceName2
```

## How to inspect and debug services
**Checking the running services logs _continuously_**
```sh
./datalab-cli.sh -a logs-follow -s serviceName1 -s serviceName2
```
**Printing out the services logs**
```sh
./datalab-cli.sh -a logs -s serviceName1 -s serviceName2
```

**Accessing a running component of a service to execute commands**
```sh
./datalab-cli.sh -a access -c componentName
```

**Checking the running services components for status and further info**
```sh
docker ps
```

## How to stop services

```sh
./datalab-cli.sh -a stop -s serviceName1 -s serviceName2
```

```sh
./datalab-cli.sh -a stop-all
```

## How to show data-cli help

```sh
./datalab-cli.sh -a help
```

## How to load files into services filesystem
Some of the available services may require to access to your laboratory VM local files for sake of simplicity.

For this purpose, you can use the directory `/home/studente/my-data` to share data with these services. They will be available into the `/data/my-data` directory of service filesystem.

The services running this feature are:

* MongoDB
* Neo4j
* ArangoDB
* Apache Nifi
* JupyterLab
