#!/usr/bin/env bash

function usage () {
    cat <<HELP_USAGE

    $0  -a help
    $0  -a <action> -s <service> -s <service>
    $0  -a access -c <component>

   -a  action to be executed (valid actions: start, stop, stop-all, logs, logs-follow, access, help)
   -s  name of the service
   -c  name of the component (only one component at time)
HELP_USAGE
}

function checkServiceDependencies () {
  local services=($@)

  if [[ ${#services[@]} -lt 1 ]]; then
      echo 'You must specify at least a service with the option -s'
      exit
  fi

  expandedServices=()
  for service in ${services[@]}; do
    case $service in
        'hbase' )
          dependencies=('hadoop' 'zookeeper')
          intersection=($(comm -12 <(printf '%s\n' "${dependencies[@]}" | LC_ALL=C sort) <(printf '%s\n' "${services[@]}" | LC_ALL=C sort)))
          if [[ ${#intersection[@]} -ne ${#dependencies[@]} ]]; then
            echo "Cannot start $service due to missing dependencies!"
            echo "Required dependencies are: ${dependencies[@]}"
            exit 1
          fi;;
        'hive' )
          dependencies=('hadoop')
          intersection=($(comm -12 <(printf '%s\n' "${dependencies[@]}" | LC_ALL=C sort) <(printf '%s\n' "${services[@]}" | LC_ALL=C sort)))
          if [[ ${#intersection[@]} -ne ${#dependencies[@]} ]]; then
            echo "Cannot start $service due to missing dependencies!"
            echo "Required dependencies are: ${dependencies[@]}"
            exit 1
          fi;;
        'kafka' )
          dependencies=('zookeeper')
          intersection=($(comm -12 <(printf '%s\n' "${dependencies[@]}" | LC_ALL=C sort) <(printf '%s\n' "${services[@]}" | LC_ALL=C sort)))
          if [[ ${#intersection[@]} -ne ${#dependencies[@]} ]]; then
            echo "Cannot start $service due to missing dependencies!"
            echo "Required dependencies are: ${dependencies[@]}"
            exit 1
          fi;;
    esac
  done
}

function expandServices () {
  local services=($@)
  if [[ ${#services[@]} -lt 1 ]]; then
      echo 'You must specify at least a service with the option -s'
      exit
  fi

  for service in ${services[@]}; do
    case $service in
        'hadoop' )
          expandedServices+=('hadoop-namenode' 'hadoop-datanode' 'hadoop-resourcemanager' 'hadoop-nodemanager' 'hadoop-historyserver');;
        'hbase' )
          expandedServices+=('hbase-master' 'hbase-region');;
        'hive' )
          expandedServices+=('hive-server' 'hive-metastore' 'hive-metastore-postgresql');;
        'mongo' )
            expandedServices+=('mongo' 'mongo-express');;
        *)
          expandedServices+=($service);;
    esac
  done
}

services=()
while getopts ":a:s:c:" opt; do
     case $opt in
        a ) action=$OPTARG;;
        s ) services+=($OPTARG);;
        c ) component=$OPTARG;;
     esac
done

case $action in
    'start' )
      checkServiceDependencies ${services[@]}
      expandServices ${services[@]}
      docker-compose up -d ${expandedServices[@]}
      exit;;
    'stop' )
      expandServices ${services[@]}
      docker-compose stop ${expandedServices[@]}
      exit;;
    'stop-all' )
      docker-compose stop;;
    'logs' )
      expandServices ${services[@]}
      docker-compose logs ${expandedServices[@]}
      exit;;
    'logs-follow' )
      expandServices ${services[@]}
      docker-compose logs --follow ${expandedServices[@]}
      exit;;
    'access' )
      echo "Accessing into $component..."
      docker exec ${component} bash &> /dev/null
      exitStatus=$?
      if [[ $exitStatus -eq 0 ]]; then
        docker-compose exec ${component} bash 2> /dev/null
      elif [[ $exitStatus -eq 126 ]]; then
        docker-compose exec ${component} sh 2> /dev/null
      else
        echo "Cannot access into $component! Please, verify it is an available and running component"
      fi;;
    'help' )
      usage
      exit;;
    *)
      echo 'Action not supported!'
      usage
      exit;;
esac
